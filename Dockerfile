FROM node:alpine
EXPOSE 8080
RUN apk update && apk upgrade && apk add --no-cache git

RUN git clone https://gitlab.com/serg4kostiuk/voting-app.git
WORKDIR /voting-app
RUN npm install

CMD ["npm", "run", "start"]